var mongoose = require('mongoose');
const bicicleta = require('../../models/bicicleta');

var Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas',function(){


    beforeEach(function(done){
        var mongodb = 'mongodb://localhost/testdb';
        mongoose.connect(mongodb,{useNewUrlParser:true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to the database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });
    describe('Bicicleta.create instance',() => {
        it ('crea una instancia de bicicleta',() =>{
            var bici = Bicicleta.createInstance(1, 'verde', 'montaña',[-34.5, -54.5]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("montaña");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.5);
        });
    });

    describe('Bicicleta.allbicis',() =>{
        it('comienza vacia',(done) =>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            });
    
        });
    });
    
    describe('Bicicleta.add',() =>{
        it('agrega una bici',(done) =>{
            var aBici = new Bicicleta({code:1, color:'verde', modelo:'urbana'});
            Bicicleta.add(aBici,function(err,newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
    
                    done();
                });
            });
        });
    
    });

    describe('Bicicleta.findByCode',()=>{
        it('Debe devolver la bici con code 1',(done) =>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1, color:'verde', modelo:'montaña'});
                Bicicleta.add(aBici,function(err,newBici){
                    if(err) console.log(err);
                    var aBici2 = new Bicicleta({code:2, color:'rojo', modelo:'urbana'});
                    Bicicleta.add(aBici2,function(err,newBici){
                        if(err) console.log(err);

                        Bicicleta.findByCode(1, function(err, targetbici){
                            expect(targetbici.code).toBe(aBici.code);
                            expect(targetbici.color).toBe(aBici.color);
                            expect(targetbici.modelo).toBe(aBici.modelo);
                            done();
                        });
                        });
                    });
                });
        });
    });
});



/*beforeEach(() =>{Bicicleta.allbicis=[];});



describe('Bicicleta.allbicis',() =>{
    it('Comienza vacio', () =>{
        expect(Bicicleta.allbicis.length).toBe(0);
    });

});


describe('Bicicleta.add',() =>{
    it('Agregamos una', () =>{
        expect(Bicicleta.allbicis.length).toBe(0);

        var a = new Bicicleta(1, 'Blanca', 'Urbana', [-38.01792,-57.58247]);
        Bicicleta.add(a);

        expect(Bicicleta.allbicis[0]).toBe(a);
    });

});


describe('Bicicleta.findById',() =>{
    it('Debe devolver la bici con ID 1', () =>{
        expect(Bicicleta.allbicis.length).toBe(0);

        var a = new Bicicleta(1, 'Roja', 'Montaña', [-38.01792,-57.58247]);
        Bicicleta.add(a);
        var b = new Bicicleta(1, 'Blanca', 'Urbana', [-38.01792,-57.58247]);
        Bicicleta.add(b);


        var targetbici = Bicicleta.findById(1);
        expect(targetbici.id).toBe(1);
        expect(targetbici.color).toBe(a.color);
        expect(targetbici.modelo).toBe(a.modelo);
    });

});*/